package biz.chooseperson;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static biz.chooseperson.R.id.sex;

public class MainActivity extends AppCompatActivity{

    EditText etName;
    EditText etSex;
    EditText etExperience;
    EditText etCompany;
    EditText etCodeLanguage;
    Button btnGenerateToClass;
    Button btnGenerateFromClass;
    TextView tvChoosePerson;

    Programmer mProgrammer;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = (EditText) findViewById(R.id.name);
        etSex = (EditText) findViewById(sex);
        etExperience = (EditText) findViewById(R.id.experience);
        etCompany = (EditText) findViewById(R.id.company);
        etCodeLanguage = (EditText) findViewById(R.id.code_language);

        btnGenerateToClass = (Button) findViewById(R.id.btnTo);
        btnGenerateFromClass = (Button) findViewById(R.id.btnFrom);
        tvChoosePerson = (TextView) findViewById(R.id.tv_choose_person);

        btnGenerateToClass.setOnClickListener(mOnClickListener);
        btnGenerateFromClass.setOnClickListener(mOnClickListener);

    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnTo:
                      /* mProgrammer = new Programmer(etName.getText().toString(),
                              etSex.getText().toString(), etExperience.getText().toString(),
                            etCompany.getText().toString(), etCodeLanguage.getText().toString());*/

                    etName.setText("");
                    etSex.setText("");
                    etExperience.setText("");
                    etCompany.setText("");
                    etCodeLanguage.setText("");
                    break;
                case R.id.btnFrom:
                    tvChoosePerson.setText(mProgrammer.getName() + "\n"
                            + mProgrammer.getSex() + "\n"
                            + mProgrammer.getExperience() + "\n"
                            + mProgrammer.getCompany() + "\n"
                            + mProgrammer.getCodeLanguage());


                    break;
            }
        }
    };

}
