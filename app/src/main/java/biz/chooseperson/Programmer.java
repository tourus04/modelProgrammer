package biz.chooseperson;

/**
 * @author Stas
 * @since 08.02.17.
 */

public class Programmer {

    private String mName;
    private String mSex;
    private String mExperience;
    private String mCompany;
    private String mCodeLanguage;

    public Programmer(String name, String sex, String experience, String company, String codeLanguage) {
        mName = name;
        mSex = sex;
        mExperience = experience;
        mCompany = company;
        mCodeLanguage = codeLanguage;
    }

    public String getName() {

        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSex() {
        return mSex;
    }

    public void setSex(String sex) {
        mSex = sex;
    }

    public String getExperience() {
        return mExperience;
    }

    public void setExperience(String experience) {
        mExperience = experience;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getCodeLanguage() {
        return mCodeLanguage;
    }

    public void setCodeLanguage(String codeLanguage) {
        mCodeLanguage = codeLanguage;
    }
}
